external ( & ) : ('a -> 'b) -> 'a -> 'b = "%apply"

module Range = struct
  let fold_left f init (from, to_) =
    let rec loop acc i =
      if i > to_ then acc
      else loop (f acc i) (i+1)
    in
    loop init from
      
  let to_list (from, to_) =
    let rec f acc x = 
      if x < from then acc
      else f (x::acc) (x-1)
    in
    f [] to_
end


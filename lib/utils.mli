external ( & ) : ('a -> 'b) -> 'a -> 'b = "%apply"
(** Same as (@@), but I prefer its precedence *)

module Range : sig
  val fold_left : ('acc -> int -> 'acc) -> 'acc -> (int * int) -> 'acc
  val to_list : (int * int) -> int list
end

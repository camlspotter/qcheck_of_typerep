open Typerep_lib.Std
open Utils

module Gen = QCheck.Gen

exception Excluded

module KeySet = Set.Make(Typename.Key)

module A = struct
  module RS = Random.State

  type 'a t = KeySet.t -> 'a Gen.sized
  let name = "Arbitrary.Gen"
  let required = []
             
  include Typerep_lib.Variant_and_record_intf.M(struct 
      type 'a t = KeySet.t -> 'a Gen.sized 
    end)
      
  let int : int t = fun _ _ -> Gen.int

  let int32 : int32 t = fun _ _ rs -> 
    let i = Gen.ui32 rs in
    let b = Gen.bool rs in
    match b with
    | true -> i
    | false -> Int32.neg i

  let int64 _ _ rs =
    let i = Gen.ui64 rs in
    let b = Gen.bool rs in
    match b with
    | true -> i
    | false -> Int64.neg i

  let nativeint _ _ rs =
    let i = RS.nativeint rs Nativeint.max_int in
    let b = Gen.bool rs in
    match b with
    | true -> i
    | false -> Nativeint.neg i
    
  let char _ _ = Gen.char
  let float _ _ = Gen.float
  let string _ sz = Gen.string_size & Gen.int_bound & max sz 1 * 5
  let bytes _ sz rs = 
    Bytes.of_string & Gen.string_size (Gen.int_bound & max sz 1 * 5) rs

  let bool : bool t = fun _ _ -> Gen.bool

  let unit _ _ = Gen.unit

  let option (f : 'a t) : 'a option t = fun ex sz rs ->
    if sz <= 0 then None
    else 
      match Gen.bool rs with
      | false -> None
      | true -> Some (f ex (sz-1) rs)

  let list (f : 'a t) : 'a list t = fun ex sz -> 
    if sz = 0 then Gen.return []
    else Gen.list_size (Gen.int_bound & max sz 1 * 3) (f ex & sz - 1)
    
  let array f ex sz = 
    if sz <= 0 then Gen.return [| |]
    else Gen.array_size (Gen.int_bound & max sz 1 * 3) (f ex & sz - 1)

  let lazy_t : 'a t -> 'a lazy_t t = fun f ex sz rs -> lazy (f ex sz rs)
    
  let ref_ f ex sz rs = ref & f ex sz rs

  (* random impure function *)      
  let function_ : 'a t -> 'b t -> ('a -> 'b) t = fun _at bt ex sz rs ->
    fun _a -> bt ex sz rs

  let tuple2 : 'a t -> 'b t -> ('a * 'b) t = fun at bt ex sz rs ->
    let sz = sz - 1 in
    let a = at ex sz rs in
    let b = bt ex sz rs in
    (a, b)
    
  let tuple3 at bt ct ex sz rs = 
    let sz = sz - 1 in
    let a = at ex sz rs in
    let b = bt ex sz rs in
    let c = ct ex sz rs in
    (a, b, c)

  let tuple4 at bt ct dt ex sz rs = 
    let sz = sz - 1 in
    let a = at ex sz rs in
    let b = bt ex sz rs in
    let c = ct ex sz rs in
    let d = dt ex sz rs in
    (a, b, c, d)
    
  let tuple5 at bt ct dt et ex sz rs = 
    let sz = sz - 1 in
    let a = at ex sz rs in
    let b = bt ex sz rs in
    let c = ct ex sz rs in
    let d = dt ex sz rs in
    let e = et ex sz rs in
    (a, b, c, d, e)
    
  let record : 'a Record.t -> 'a t = fun r ex sz rs ->
    Record.create r { get = fun f -> Field.traverse f ex (sz - 1) rs }

  let variant : 'a Variant.t -> 'a t = fun v ex sz rs ->
    let try_ith ex i =
      match Variant.tag v i with
      | Tag tag -> 
          match Tag.create tag with
          | Const v -> v
          | Args f -> f & Tag.traverse tag ex (sz - 1) rs
    in
    let len = Variant.length v in

    if sz > 0 then try_ith ex & RS.int rs len
    else begin
      (* sz <= 0, we cannot recurse for ever! *)
      if KeySet.mem (Typename.key & Variant.typename_of_t v) ex then begin
        raise Excluded;
      end;
      let nullaries = 
        (* If there are 0 ary constructors, we choose one of them *)
        let f acc i = 
          let Tag tag = Variant.tag v i in
          if Tag.arity tag = 0 then i::acc else acc
        in
        Range.fold_left f [] (0, len-1)
      in
      match nullaries with
      | _::_ ->
          try_ith ex & List.nth nullaries & RS.int rs & List.length nullaries
      | [] -> 
          (* otherwise, we continue but adding the type to the exclusion set *)
          let ex = KeySet.add (Typename.key & Variant.typename_of_t v) ex in
          let is = Gen.shuffle_l (Range.to_list (0, len-1)) rs in
          let rec f = function
            | [] -> raise Excluded
            | i::is ->
                try 
                  try_ith ex i
                with
                | Excluded -> f is
          in
          f is
    end

  module Named = Type_generic.Make_named_for_closure(struct
      type 'a input = KeySet.t
      type 'a output = int -> RS.t -> 'a
      type 'a t = 'a input -> 'a output
    end)
end

module type S = Type_generic.S with type 'a t = KeySet.t -> 'a Gen.sized

type override = (module S) -> unit

let of_typerep ?override tr = 
  let module M = Type_generic.Make(A) in
  begin match override with
    | None -> ()
    | Some f -> f (module M : S)
  end;
  let `generic f = M.of_typerep tr in
  fun ?(exclude=KeySet.empty) -> f exclude

module type S = Typerep_lib.Type_generic.S 
  with type 'a t = 'a QCheck.Shrink.t

type override = (module S) -> unit

(** [of_typerep ?override typerep] derives a shrink of a type, 
    whose runtime information is given by [typerep].
    
    [override] is a function to override shrinks of specific types.
    For this purpose, you can use functions [register0], .., [register5] 
    and [regiter] of the first class module argument.
*)
val of_typerep 
  : ?override: override
  -> 'a Typerep_lib.Std.Typerep.t 
  -> 'a QCheck.Shrink.t

exception Excluded
(** Exception raised when a value generator finds impossible to generate 
    a value.
*)

module KeySet : Set.S with type elt = Typerep_lib.Typename.Key.t
(** Set of type names *)                                        

module type S = Typerep_lib.Type_generic.S 
  with type 'a t = KeySet.t -> 'a QCheck.Gen.sized

type override = (module S) -> unit

(** [of_typerep ?override typerep] derives an random value generator 
    of a type, whose runtime information is given by [typerep].
    
    [override] is a function to override generators of specific types.
    For this purpose, you can use functions [register0], .., [register5] 
    and [regiter] of the first class module argument.
    
    You can use [exclude] to exclude types used in the value generaiton.
    If there is no way to produce a value using none of excluded types,
    the generator raises an exception [Excluded].

    [size] is a hint to limit the maxium recursion depth of the value
    generation.  Note that this is not the exact depth limit: the generation
    may recurse more than [size] times.
    
    [rs] is a random state used for the generation.
*)
val of_typerep 
  : ?override: override
  -> 'a Typerep_lib.Std.Typerep.t 
  -> ?exclude: KeySet.t
  -> 'a QCheck.Gen.sized

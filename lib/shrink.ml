open Typerep_lib.Std
open Utils

module Sh = QCheck.Shrink
module Iter = QCheck.Iter
module RS = Random.State

exception Excluded

module KeySet = Set.Make(Typename.Key)

module A = struct
  type 'a t = 'a Sh.t
  let name = "Arbitrary.Shrink"
  let required = []
             
  include Typerep_lib.Variant_and_record_intf.M(struct 
      type nonrec 'a t = 'a t
    end)
      
  let int : int t = Sh.int

  let int32 : int32 t = fun x yield ->
    let y = ref x in
    while !y < -2l || !y > 2l do y := Int32.div !y 2l; yield !y; done;
    if x > 0l then yield (Int32.sub x 1l);
    if x < 0l then yield (Int32.add x 1l);
    ()

  let int64 x yield = 
    let y = ref x in
    while !y < -2L || !y > 2L do y := Int64.div !y 2L; yield !y; done;
    if x > 0L then yield (Int64.sub x 1L);
    if x < 0L then yield (Int64.add x 1L);
    ()

  let nativeint x yield =
    let open Nativeint in
    let y = ref x in
    while !y < -2n || !y > 2n do y := div !y 2n; yield !y; done;
    if x > 0n then yield (sub x 1n);
    if x < 0n then yield (add x 1n);
    ()
    
  let char = Sh.char
  let float x yield =
    let y = ref x in
    while !y < -2.0 || !y > 2.0 do y := !y /. 2.0; yield !y; done

  let string = Sh.string
  let bytes x = Iter.map Bytes.of_string & Sh.string & Bytes.to_string x
  let bool = Sh.nil
  let unit = Sh.unit
  let option = Sh.option
  let list sh x = Sh.list ~shrink:sh x
  let array sh x = Sh.array ~shrink:sh x
  let lazy_t sh x = Iter.map (fun x -> lazy x) & sh & Lazy.force x
  let ref_ sh x = Iter.map ref & sh !x

  (* random impure function *)      
  let function_ : 'a t -> 'b t -> ('a -> 'b) t = fun _ -> assert false
    (* hard to give the default shrinker *)

  let tuple2 : 'a t -> 'b t -> ('a * 'b) t = Sh.pair
  let tuple3 = Sh.triple
  let tuple4 = Sh.quad
  let tuple5 at bt ct dt et (a,b,c,d,e) =
    let open Iter in
    at a >>= fun a ->
    bt b >>= fun b ->
    ct c >>= fun c ->
    dt d >>= fun d ->
    et e >>= fun e -> return (a,b,c,d,e)

  type 'record fieldValue = FieldValue : (('record, 'field) Field.t * 'field) -> 'record fieldValue

  let eq_tyid (type a) (type b) (at : ('r, a) Field.t) (bt : ('r, b) Field.t) =
    Typename.same_witness_exn (Field.tyid at) (Field.tyid bt)

  let iter_sequence : 'a . 'a Iter.t list -> 'a list Iter.t = fun is ->
    let open Iter in
    let rec f acc = function
      | [] -> map List.rev acc
      | i :: is -> f (i >>= fun x -> 
                      acc >>= fun xs -> 
                      return (x :: xs)) is
    in
    f (return []) is

  let record : 'a Record.t -> 'a -> 'a Iter.t = fun r a -> 
    let open Iter in
    let g f = 
      Iter.map (fun x -> (Field.index f, FieldValue (f, x))) 
        Field.(traverse f & get f a)
    in
    let ifvis = Record.fold r ~init:[] ~f:(fun acc (Field f) -> g f :: acc) in
    let ifvsi = iter_sequence ifvis in
    ifvsi >>= fun ifvs -> 
    let get f =
      let ind = Field.index f in
      let FieldValue (f', v) = List.assoc ind ifvs in
      let eq = eq_tyid f' f in
      Type_equal.conv eq v
    in
    return & Record.create r { get }

  let variant : 'a Variant.t -> 'a -> 'a Iter.t = fun v a -> 
    let Value (tag, args) = Variant.value v a in
    match Tag.create tag with
    | Const v -> Iter.empty
    | Args f -> Iter.map f & Tag.traverse tag args

  module Named = Type_generic.Make_named_for_closure(struct
      type 'a input = 'a
      type 'a output = 'a Iter.t
      type 'a t = 'a input -> 'a output
    end)
end

module type S = Type_generic.S with type 'a t = 'a Sh.t

type override = (module S) -> unit

let of_typerep ?override tr = 
  let module M = Type_generic.Make(A) in
  begin match override with
    | None -> ()
    | Some f -> f (module M : S)
  end;
  let `generic f = M.of_typerep tr in
  f

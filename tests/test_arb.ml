open Typerep_lib.Std
open Ocaml_conv.Default

type t = 
  | Foo 
  | Bar of t 
  | Boo of int 
  | Zee of t * t
  | Pee of t list
  | Nee of (t * t)
  [@@deriving typerep, conv{ocaml}]

let () = Random.self_init ()

let () = 
  let comp = QCheck_of_typerep.Gen.of_typerep typerep_of_t  in
  let rs = Random.get_state () in
  Format.eprintf "%a@." (Camlon.Ocaml.format_with ocaml_of_t) (comp 3 rs)


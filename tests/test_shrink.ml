open Typerep_lib.Std
open Ocaml_conv.Default

type t = int [@@deriving typerep, conv{ocaml}]

let () = Random.self_init ()

let () = 
  let comp = QCheck_of_typerep.Shrink.of_typerep typerep_of_t  in
  let i = comp 5 in
  let rec loop () =
    i (fun x -> 
        Format.eprintf "%a@." 
          (Camlon.Ocaml.format_with ocaml_of_t)
          x);
    loop ()
  in
  loop ()

